# Copyright 2007 Bryan Østergaard <bryan.ostergaard@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require gnu [ suffix=tar.xz ] \
    flag-o-matic \
    bash-completion \
    autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]

if ever is_scm ; then
    DOWNLOADS=""
    SCM_REPOSITORY="https://git.savannah.gnu.org/git/${PN}.git"
    require scm-git
fi

export_exlib_phases pkg_setup src_configure src_prepare src_install pkg_postinst

SUMMARY="GRUB (Grand Unified Boot) bootloader"

LICENCES="GPL-3"
MYOPTIONS="
    device-mapper [[ description = [ Ability to detect and use device-mapper devices ] ]]
    efi           [[ description = [ Build for EFI platform ] ]]
    grub-mount    [[ description = [ FUSE driver for filesystems that GRUB understands ] ]]
    mkfont        [[ description = [ Build grub-mkfont which can create GRUB font files ] ]]
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

# TODO: needs qemu-system-i386
RESTRICT="test"

DEPENDENCIES="
    build:
        sys-apps/help2man [[ note = [ man pages won't get created otherwise resulting in empty man dirs ] ]]
        sys-devel/bison
        sys-devel/flex[>=2.5.35]
        sys-devel/make
        device-mapper? ( sys-fs/lvm2 )
        grub-mount? ( sys-fs/fuse:0 )
        mkfont? (
            fonts/unifont
            media-libs/freetype:2
        )
    build+run:
        app-arch/xz
        sys-devel/gettext [[ note = [ required for grub-mkconfig, grep gettext in /etc/grub.d ] ]]
        !sys-boot/grub-static [[ description = [ /sbin/grub-install collides ] resolution = manual ]]
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
    run:
        efi? ( sys-boot/efibootmgr )
    suggestion:
        sys-boot/os-prober [[
            description = [ Ability to add other OSs automatically to grub.cfg via grub-mkconf ]
        ]]
"

AT_M4DIR=( m4 )

grub_pkg_setup() {
    # grub2 should be compiled with -Os, other optimizations may result in oversized core images
    replace-flags -O* -Os
    # retpoline flag is incompatible with the -mcmodel=large flag used by grub
    filter-flags -mindirect-branch=thunk
}

grub_src_prepare() {
    edo sed \
        -e "s/.*Running autoreconf.*/exit 0/" \
        -i autogen.sh

    edo ./autogen.sh

    autotools_src_prepare
}

grub_src_configure() {
    filter-flags -mfloat-abi=hard

    myconf=(
        "CPP=${CC} -E"
        BUILD_CC=$(exhost --build)-cc
        BUILD_CFLAGS="$(print-build-flags CFLAGS)"
        BUILD_CPPFLAGS="$(print-build-flags CPPFLAGS)"
        BUILD_LDFLAGS="$(print-build-flags LDFLAGS)"
        --with-bootdir=/boot
        --with-grubdir=grub
        --enable-cache-stats
        --disable-libzfs
        --disable-werror
        # TODO: for now disable emulation utils
        --disable-efiemu
        --disable-grub-emu-sdl
        --disable-grub-emu-pci
        --disable-mm-debug
        gt_cv_func_gnugettext{1,2}_libc=yes
    )

    option efi && myconf+=( '--with-platform=efi' )

    econf "${myconf[@]}" \
        $(option_enable device-mapper) \
        $(option_enable grub-mount) \
        $(option_enable mkfont grub-mkfont)
}

grub_src_install() {
    default

    insinto /etc/default
    newins "${FILES}"/grub.default grub

    dodoc "${WORK}"/docs/grub.cfg

    edo rm -f "${IMAGE}"/usr/$(exhost --target)/lib/charset.alias

    edo rm "${IMAGE%/}"/etc/bash_completion.d/${PN}
    edo rmdir "${IMAGE%/}"/etc/bash_completion.d/
    dobashcompletion "${WORK%/}"/util/bash-completion.d/${PN}
}

grub_pkg_postinst() {
    elog "Use grub-install /dev/your-device e.g. sda to install grub onto the"
    elog "device you wish to boot from."
    elog "A sample grub.cfg has been installed into /usr/share/doc/${PNVR}."
    elog "Copy this to /boot/grub and edit it to fit your configuration."
    elog "Alternatively, you can use the provided grub-mkconfig utility."
}

