# Copyright 2009-2010 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]

SUMMARY="A library implementing the SSH2 protocol"
HOMEPAGE="https://www.${PN}.org"
DOWNLOADS="${HOMEPAGE}/download/${PNV}.tar.gz"

REMOTE_IDS="freecode:${PN}"

UPSTREAM_CHANGELOG="${HOMEPAGE}/changes.html [[ lang = en ]]"
UPSTREAM_DOCUMENTATION="${HOMEPAGE}/docs.html [[ lang = en ]]"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS="
    ( providers: gcrypt libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build+run:
        sys-libs/zlib
        providers:gcrypt? ( dev-libs/libgcrypt )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PNV}-fix-crypto-autodetection.patch
)

src_prepare() {
    # keyboard-interactive tests fail: Failed requesting pty
    edo sed -e "s:ssh2.sh::" \
        -i tests/Makefile.am

    autotools_src_prepare
}

src_configure() {
    local params=(
        --enable-hidden-symbols
        --disable-examples-build
        --disable-clear-memory
        --disable-static
        --with-libz
    )

    if option providers:gcrypt; then
        params+=( --with-crypto=libgcrypt )
    else
        params+=( --with-crypto=openssl )
    fi

    econf "${params[@]}"
}

