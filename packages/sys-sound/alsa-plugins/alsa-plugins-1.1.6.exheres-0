# Copyright 2008 Alexander Færøy <eroyf@exherbo.org>
# Copyright 2009 Bo Ørsted Andresen <zlin@exherbo.org>
# Copyright 2009 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'alsa-plugins-1.0.19.ebuild' from Gentoo, which is:
#     Copyright 1999-2009 Gentoo Foundation

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ] flag-o-matic

SUMMARY="The Advanced Linux Sound Architecture (ALSA) Plugins"
HOMEPAGE="https://www.alsa-project.org"
DOWNLOADS="mirror://alsaproject/${PN#alsa-}/${PNV}.tar.bz2"

LICENCES="GPL-2 LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    ffmpeg
    jack
    libsamplerate
    pulseaudio
    speex

    ffmpeg? ( ( providers: ffmpeg libav ) [[ number-selected = exactly-one ]] )
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        sys-sound/alsa-lib[>=1.0.25]
        ffmpeg? (
            providers:ffmpeg? ( media/ffmpeg )
            providers:libav? ( media/libav )
        )
        jack? ( media-sound/jack-audio-connection-kit[>=0.98] )
        libsamplerate? ( media-libs/libsamplerate )
        pulseaudio? ( media-sound/pulseaudio[>=0.9.11] )
        speex? ( media-libs/speex[>=1.2_rc] )
"

AT_M4DIR=( m4 )

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/e8fabec7adc70220f52588dc170d90d146b92ba7.patch
    "${FILES}"/6e40eb5fd346207021a95d06bc30205a537926ea.patch
    "${FILES}"/cc6bed233a3167d806834460befca2c6d655f0fb.patch
    "${FILES}"/24db7f59d76984e2901f2834a297735853cab776.patch
    "${FILES}"/4afd4ab0b276b26b965bae3aadaa31cdb52b1df0.patch
    "${FILES}"/beb24e58763e3b1d831fcd7ef87a478e6ac74fcc.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-arcamav
    --enable-mix
    --enable-oss
    --enable-usbstream
    --disable-maemo-plugin
    --disable-maemo-resource-manager
    --disable-static
    --with-alsadatadir=/usr/share/alsa
    --with-alsagconfdir=/usr/share/alsa/alsa.conf.d
    --with-alsalconfdir=/etc/alsa/conf.d
    --with-plugindir=/usr/$(exhost --target)/lib/alsa-lib
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'ffmpeg a52'
    'ffmpeg lavrate'
    'ffmpeg libav'
    jack
    'libsamplerate samplerate'
    pulseaudio
    'speex speexdsp'
)

src_prepare() {
    autotools_src_prepare

    # NOTE(somasis): rpath is needed to fix dlopen errors on musl
    append-flags "-Wl,-rpath=/usr/$(exhost --target)/lib/alsa-lib" "-DHAVE_STDINT_H"
}

src_install() {
    default

    # set the pulseaudio plugin as default output for applications using alsa
    if option pulseaudio ; then
        edo mv "${IMAGE}"/etc/alsa/conf.d/99-pulseaudio-default.conf{.example,}
    fi

    edo cd doc
    dodoc README-arcam-av README-pcm-oss upmix.txt vdownmix.txt
    option ffmpeg && dodoc a52.txt lavrate.txt
    option jack && dodoc README-jack
    option libsamplerate && dodoc samplerate.txt
    option pulseaudio && dodoc README-pulse
    option speex && dodoc speexdsp.txt speexrate.txt
}

